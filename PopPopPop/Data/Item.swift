//
//  Item.swift
//  PopPopPop
//
//  Created by user143580 on 8/10/18.
//  Copyright © 2018 diphone. All rights reserved.
//

import Foundation

class Item: Decodable {
    let id: Int
    let name: String
    let imageUrl: String
}

struct Todo: Codable{
    let userId: String
    let id: String
    let title: String
    let completed: String
}
